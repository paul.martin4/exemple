# AAAA | Nom de la structure bénéficiaire x Latitudes

_Ce repository est un exemple pour illustrer l'arborescence à utiliser et la documentation à mettre en place. Vous avez également à votre disposition un repo exemple pour comprendre comment mettre en application ces conseils : [1617_voxe](https://gitlab.com/latitudes-exploring-tech-for-good/voxe/1617_voxe)._

## Contexte
* Présentation rapide de la structure bénéficiaire.
* La mission sociale en une phrase.
* Un chiffre impactant sur son activité aujourd'hui.
* Lien vers le site de la structure bénéficiaire.

## À propos du projet
* Enjeu pour la structure : montrer ici l'impact direct du projet sur sa mission sociale.
* Les objectifs du projet tels que définis à la scope review.
* Potentiels liens vers un/des livrable(s) (pour télécharger l'app ou aller sur le site produit par exemple).
* Le cadre dans lequel cela s'inscrit : Tech for Good Explorers (description à copier-coller directement).

## Perspectives d'évolution
* Notez ici les pistes d'évolutions auquels vous avez pensé, que vous suggéreriez à des personnes qui souhaiteraient reprendre le projet.

## Sommaire
* Le sommaire du repo complet, des textes de descriptions vous sont fournis, copiez-collez-les.

## Contact
* Les contacts des étudiant.e.s, mentors et porteur.se.s de projets (mail et/ou numéro de téléphone selon vos préférences).
